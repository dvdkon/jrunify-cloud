#!/usr/bin/env bash
cd $(dirname $(readlink -f $0))

outdir="/var/www/$(date +%Y-%m-%d)"
while [ -e "$outdir" ]; do
	outdir_num="$(echo "$outdir" | sed -nEe 's/.*_(.*)$/\1/gp')"
	outdir_num="${outdir_num:-0}"
	outdir_num="$((outdir_num + 1))"
	outdir_orig="$outdir"
	outdir="$(echo "$outdir" | sed -nEe 's/_.*$/_'$outdir_num'/gp')"
	outdir="${outdir:-${outdir_orig}_${outdir_num}}"
done
mkdir "$outdir"
if ../batch_scripts/run.py "$outdir"
then
	[ -e /var/www/latest ] && rm /var/www/latest
	ln -s "$outdir" /var/www/latest
fi
