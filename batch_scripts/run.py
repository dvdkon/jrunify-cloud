#!/usr/bin/env python3
import os
import sys
import time
import shutil
import pathlib
import datetime as dt
import jinja2
import gcloud as cloud

ROOT = pathlib.Path(os.path.dirname(os.path.realpath(__file__)))
STATUS_HTML_OUTPATH = "task_status_out.html"

checkpoint_times = {}

status_html_template = jinja2.Template((ROOT / "task_status.html").read_text())

def checkpoint(name):
    print("Checkpoint reached:", name)
    checkpoint_times[name] = dt.datetime.now()
    start_time = checkpoint_times["start"]
    checkpoints_text = \
        {n: str(t - start_time)
         for n, t in checkpoint_times.items()}
    status_html_template \
        .stream(checkpoints=checkpoints_text,
                start_time=dt.datetime.isoformat(start_time)) \
        .dump(str(status_html_out))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: {} <outdir>".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    outdir = pathlib.Path(sys.argv[1])
    status_html_out = outdir / STATUS_HTML_OUTPATH
    checkpoint("start")
    cloud.create_batch_vm()
    checkpoint("vm_setup")

    try:
        c = cloud.get_ssh_connection()
        c.put(str(ROOT / "batch_task.sh"))
        c.run("chmod +x batch_task.sh")
        c.run("mkfifo task_out")
        c.run("dtach -n task_dtach_sock ./batch_task.sh")
        while "task_end" not in checkpoint_times:
            try:
                cat = c.run("cat task_out", hide=True)
                for line in cat.stdout.split("\n"):
                    line = line.strip()
                    if line == "": continue
                    checkpoint(line)
                time.sleep(1)
            except:
                print("Connection failure, trying to reconnect")
                cloud.wakeup_batch_vm()
                c = cloud.get_ssh_connection()

        c.run("tar cf logs.tar logs")
        logs_tarfile = outdir / "logs.tar"
        c.get("logs.tar", str(logs_tarfile))
        shutil.unpack_archive(str(logs_tarfile), str(outdir))
        pg_logfile = outdir / "logs" / "pg_log"
        c.get("db/logfile", str(pg_logfile))
        pg_logfile.chmod(0o644)
        logs_tarfile.unlink()

        c.run("cd gtfs_merged; zip -r ../gtfs_merged.zip *")
        c.get("gtfs_merged.zip", str(outdir / "gtfs_merged.zip"))
    finally:
        cloud.delete_batch_vm()

        checkpoint("end")
