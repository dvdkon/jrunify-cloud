#!/usr/bin/env bash

JRUTIL_GIT_URL="https://gitlab.com/dvdkon/jrutil.git"
DB_PATH="$PWD/db"
#DB_PATH="/tmp/jrutil_db/db" # TODO: Automount tmpfs
export PATH=/usr/lib/postgresql/10/bin:"$PATH"
CHECKPOINT_FILE=checkpoints
LOGDIR=logs
echo -n > "$CHECKPOINT_FILE"
[ -d "$LOGDIR" ] && rm -r "$LOGDIR"
mkdir -p "$LOGDIR"

#orig_stdout=$(readlink -f /proc/$$/fd/1)
checkpoint() {
	echo $1 >> task_out
	echo $1 >> "$CHECKPOINT_FILE"
	echo "-- CHECKPOINT $1"
}

#grep_jobs() {
#	jobs | sed -nEe 's|\[([0-9]+)\][+-]? +[a-zA-Z]+ +'"$*"'.*|\1|p'
#}

#wait_job_grep() {
#	wait %$(grep_jobs "$*")
#}

wait_checkpoint() {
	while :; do
		if grep -q $1 "$CHECKPOINT_FILE"; then break; fi
		sleep 1
	done
}

log_to() {
	cat > "$LOGDIR/$1"
}

log_to_exec() {
	exec > "$LOGDIR/$1" 2>&1
}

init_pg() {
	dbpath="$1"
	dbname="$2"
	[ -z "$dbpath" ] && echo "dbpath must not be empty" && exit 1
	[ -e "$dbpath/stop.sh" ] && "$dbpath/stop.sh"
	[ -e "$dbpath" ] && rm -r "$dbpath"

	mkdir -p "$dbpath"
	initdb -D "$dbpath"
	mkdir -p "$dbpath/sock"
	cat >> "$dbpath/postgresql.conf" << EOF
unix_socket_directories = '$(readlink -f $dbpath/sock)'

shared_buffers = 3GB
work_mem = 256MB
maintenance_work_mem = 1024MB
effective_cache_size = 8GB
max_wal_size = 2GB

fsync = off
synchronous_commit = off
full_page_writes = off

# TODO: Does this help?
wal_writer_delay = 1000ms
wal_writer_flush_after = 100MB
EOF

	cat > "$dbpath/start.sh" << EOF
#!/usr/bin/env sh
dbpath="`dirname $0`"
pg_ctl -D "$dbpath" -l "$dbpath/logfile" start
EOF
	chmod +x "$dbpath/start.sh"
	cat > "$dbpath/stop.sh" << EOF
#!/usr/bin/env sh
dbpath="`dirname $0`"
pg_ctl -D "$dbpath" stop
EOF
	chmod +x "$dbpath/stop.sh"

	"$dbpath/start.sh"
	createdb -h "$(readlink -f $dbpath/sock)" $dbname
	psql -h "$(readlink -f $dbpath/sock)" -c "CREATE EXTENSION pg_trgm"

	checkpoint init_pg
}

jrunify() {
	dotnet ~/jrutil/jrunify/bin/Release/*/jrunify.dll "$@"
}

build_jrutil() {
	export DOTNET_SKIP_FIRST_TIME_EXPERIENCE=true

	[ -d jrutil ] || git clone --recurse-submodules -b master "$JRUTIL_GIT_URL"
	pushd jrutil
	dotnet publish -c Release
	popd

	checkpoint jrutil_build
}

download_cisjr_data() {
	mkdir cisjr_data
	pushd cisjr_data
	curl ftp://ftp.cisjr.cz/JDF/JDF.zip -o JDF_bus.zip
	curl ftp://ftp.cisjr.cz/draha/mestske/JDF.zip -o JDF_mhd.zip
	wget --recursive --no-parent --no-host-directories --cut-dirs 2 \
		ftp://ftp.cisjr.cz/draha/celostatni/szdc/$(date +%Y)/
	wget ftp://ftp.cisjr.cz/seznamy/zastavky.csv -O zastavky.csv
	popd

	checkpoint cisjr_download
}

download_dmplj_data() {
	wget http://www.dpmlj.cz/gtfs.zip -O dpmlj.zip

	checkpoint dpmlj_download
}

extract_cisjr_data() {
	pushd cisjr_data
	unar JDF_bus.zip
	pushd JDF_bus
	for file in *.zip; do unar $file; done
	popd

	unar JDF_mhd.zip
	pushd JDF_mhd
	for file in *.zip; do unar $file; done
	popd

	pushd szdc/*
	for file in *.zip; do unar $file; done
	popd
	popd

	checkpoint cisjr_extract
}

process_cisjr_data() {
	# TODO: Configurable parallelisation
	jrunify \
		--connstr="Host=$DB_PATH/sock" \
		--out=gtfs_merged \
		--jdf-bus=cisjr_data/JDF_bus \
		--jdf-mhd=cisjr_data/JDF_mhd \
		--czptt-szdc=cisjr_data/szdc \
		--cis-stop-list=cisjr_data/zastavky.csv \
		--dpmlj-gtfs=dpmlj.zip

	checkpoint jrutil_merge_gtfs
}

log_to_exec general
build_jrutil 2>&1 | log_to build_jrutil &
init_pg "$DB_PATH" jrutil 2>&1 | log_to init_pg &
download_cisjr_data 2>&1 | log_to download_cisjr_data
download_dpmlj_data 2>&1 | log_to download_dpmlj_data
extract_cisjr_data 2>&1 | log_to extract_cisjr_data
wait_checkpoint jrutil_build
wait_checkpoint init_pg
process_cisjr_data 2>&1 | log_to process_cisjr_data
#"$DB_PATH/stop.sh"
checkpoint task_end
