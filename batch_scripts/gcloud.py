#!/usr/bin/env python3
import time
import tempfile
import os
import shutil
import pathlib
import atexit
import secrets
import subprocess as spc
import fabric
import paramiko
import googleapiclient.discovery

import config

PROJECT = config.GCLOUD_PROJECT
REGION = config.GCLOUD_REGION
ZONE = config.GCLOUD_ZONE

BATCH_VM_NAME = "jrutil-batch"

compute = googleapiclient.discovery.build("compute", "v1")

def wait_for_operation(operation):
    while True:
        result = compute.zoneOperations().get(
            project=PROJECT, zone=ZONE, operation=operation["name"]).execute()

        if result["status"] == "DONE":
            if "error" in result: raise Exception(result["error"])
            return result

        time.sleep(1)

def generate_ssh_key():
    global SSH_KEY_DIR
    SSH_KEY_DIR = pathlib.Path(tempfile.mkdtemp())
    print("SSH_KEY_DIR set to", SSH_KEY_DIR)
    #atexit.register(lambda: shutil.rmtree(SSH_KEY_DIR))

    spc.run(["ssh-keygen", "-P", "", "-t", "ed25519",
             "-f", str(SSH_KEY_DIR / "id")],
            check=True, stdout=spc.DEVNULL)

def generate_ssh_pw():
    # Paramiko seems to have some problems with keys when used on GCP VMs,
    # so let's use passwords, because I've failed at debugging the problem
    global SSH_PASSWORD
    SSH_PASSWORD = secrets.token_hex()[:16]

def get_ssh_connection():
    info = compute.instances() \
        .get(project=PROJECT, zone=ZONE, instance=BATCH_VM_NAME) \
        .execute()
    if os.environ.get("GCLOUD_INTERNAL") == "1":
        ipaddr = info["networkInterfaces"][0]["networkIP"]
    else:
        ipaddr = info["networkInterfaces"][0]["accessConfigs"][0]["natIP"]
    return fabric.Connection(ipaddr, user="jrutil", connect_kwargs={
        #"key_filename": str(SSH_KEY_DIR / "id")
        "password": SSH_PASSWORD
    })

def create_batch_vm():
    generate_ssh_key()
    generate_ssh_pw()

    image = compute.images().getFromFamily(
        project="ubuntu-os-cloud", family="ubuntu-minimal-1804-lts").execute()

    startup_script = f"""\
#!/usr/bin/env bash
if [ -f /setup_done ]; then exit 0; fi

useradd -m -s /bin/bash -G google-sudoers jrutil
pushd /home/jrutil
mkdir .ssh
chmod 700 .ssh
touch .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
chown -R jrutil:jrutil .ssh
cat > .ssh/authorized_keys << EOF
{(SSH_KEY_DIR / "id.pub").read_text()}
EOF
popd

sed -ie 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
echo "jrutil:{SSH_PASSWORD}" | chpasswd

export DEBIAN_FRONTEND=noninteractive

# Taken directly from Microsoft's .NET Core page
wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb
apt-get update
apt-get -y install apt-transport-https
apt-get -y install dotnet-sdk-2.2

# This is Mono's FSharp
apt-get -y install fsharp

apt-get -y install postgresql
systemctl disable --now postgresql

apt-get -y install git dtach unar zip

apt-get -y install uswsusp
mem_total="$(free | head -n 2 | tail -n 1 | tr -s ' ' | cut -d ' ' -f 2)"
fallocate -l "$mem_total"KiB /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo "/swapfile none swap defaults 0 0" >> /etc/fstab
resume_offset="$(swap-offset /swapfile | cut -d ' ' -f 4)"
echo 'GRUB_CMDLINE_LINUX="$GRUB_CMDLINE_LINUX resume=/dev/sda1 resume_offset='$resume_offset'"' > /etc/default/grub.d/90-hibernation.cfg
update-grub
echo "HandlePowerKey=hibernate" >> /etc/systemd/logind.conf

mkdir -p /tmp/jrutil_db
echo "none /tmp/jrutil_db tmpfs size=10G 0 0" >> /etc/fstab

touch /setup_done
systemctl reboot
"""

    # TODO: Find the ideal machine config from a price-to-performance
    # standpoint
    config = {
        "name": BATCH_VM_NAME,
        "zone": f"projects/{PROJECT}/zones/{ZONE}",
        "machineType":
            #f"projects/{PROJECT}/zones/{ZONE}/machineTypes/custom-4-13312",
            f"projects/{PROJECT}/zones/{ZONE}/machineTypes/n1-standard-4",
        "metadata": {
            "items": [
                {
                    "key": "startup-script",
                    "value": startup_script
                }
            ]
        },
        "disks": [
            {
                "boot": True,
                "autoDelete": True,
                "initializeParams": {
                    "sourceImage": image["selfLink"],
                    "diskType":
                        f"projects/{PROJECT}/zones/{ZONE}/diskTypes/pd-ssd",
                    "diskSizeGb": "50"
                },
            }
        ],
        "networkInterfaces": [
            {
                "subnetwork":
                    f"projects/{PROJECT}/regions/{REGION}/subnetworks/default",
                "accessConfigs": [
                    {
                        "name": "External NAT",
                        "type": "ONE_TO_ONE_NAT",
                        "networkTier": "PREMIUM"
                    }
                ],
                "aliasIpRanges": []
            }
        ],
        "scheduling": {
            "preemptible": True,
            # TODO: This will cost a lot of money!
            #"preemptible": False,
            "automaticRestart": False,
        },
    }
    op = compute.instances() \
        .insert(project=PROJECT, zone=ZONE, body=config) \
        .execute()
    wait_for_operation(op)

    for i in range(100):
        try:
            with get_ssh_connection() as conn:
                conn.open()
                res = conn.run("while [ ! -f /setup_done ]; do sleep 1; done; sleep 10; echo Ready")
                if res.stdout.strip() == "Ready":
                    break
        except Exception as e: # The exception hierarchy is quite messy
            time.sleep(5)

def stop_batch_vm():
    op = compute.instances() \
        .stop(project=PROJECT, zone=ZONE, instance=BATCH_VM_NAME) \
        .execute()
    wait_for_operation(op)

def delete_batch_vm():
    op = compute.instances() \
        .delete(project=PROJECT, zone=ZONE, instance=BATCH_VM_NAME) \
        .execute()
    wait_for_operation(op)

def wakeup_batch_vm():
    op = compute.instances() \
        .start(project=PROJECT, zone=ZONE, instance=BATCH_VM_NAME) \
        .execute()
    wait_for_operation(op)


if __name__ == "__main__":
    #print("Creating VM...")
    create_batch_vm()
    #input("VM created!")
    #delete_batch_vm()
    #print("Removing VM...")
    #print("VM removed!")
    #wakeup_batch_vm()
